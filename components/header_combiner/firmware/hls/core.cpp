#include "core.h"
#ifndef __SYNTHESIS__
#include <iostream>
#endif

void header_combiner(
	hls::stream<ap_axis_str> &header_stream,
	hls::stream<ap_axis_str> &data_stream,
	hls::stream<ap_axis_str> &output_stream){
	#pragma HLS INTERFACE axis port=output_stream
	#pragma HLS INTERFACE axis port=header_stream
	#pragma HLS INTERFACE axis port=data_stream
	#pragma HLS INTERFACE ap_ctrl_none port=return

	for (int i=0; i<NHEAD; i++){
		output_stream.write(header_stream.read());
	}

	for (int i=0; i<MAXSAMPLES; i++){
		if (!data_stream.empty()){
			output_stream.write(data_stream.read());
		}
	}
}
