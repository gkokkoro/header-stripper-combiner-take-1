#include "core.h"
#include <iostream>
#define NTESTSAMPLES 18

int main(){
	std::cout<<"\n===========================\n";
	hls::stream<ap_axis_str> output_stream;
	hls::stream<ap_axis_str> header_stream;
	hls::stream<ap_axis_str> data_stream;

	std::vector<word> data{43520,16384,21845,21845,21845,21845};
	std::vector<word> user{0,0,0,0,0,0};
	std::vector<word> last{0,0,0,0,0,1};

	for (int i=0; i<NTESTSAMPLES; i++){
		data.push_back(i);
		user.push_back(((i+1)%6==0)?1:0);
		last.push_back(0);
	}

	ap_axis_str write_value;
	write_value.keep = 3;
	for (int i=0; i<(NHEAD); i++){
		write_value.data = data[i];
		write_value.user = user[i];
		write_value.last = last[i];
		header_stream.write(write_value);
	}

	for (int i=NHEAD; i<(NHEAD+NTESTSAMPLES); i++){
		write_value.data = data[i];
		write_value.user = user[i];
		write_value.last = last[i];
		data_stream.write(write_value);
	}
	write_value.data = 0;
	write_value.user = 0;
	write_value.keep = 0;
	write_value.last = 1;
	data_stream.write(write_value);

	header_combiner(header_stream, data_stream, output_stream);

	while(!output_stream.empty()) {
		ap_axis_str strm_val_out;
		output_stream	.read(strm_val_out);
		std::cout<<strm_val_out.data;
			if (strm_val_out.last) {
				std::cout<<"\n";
			}else if (strm_val_out.user){
				std::cout<<" || ";
			} else {
				std::cout<<", ";
			}
	}
}
