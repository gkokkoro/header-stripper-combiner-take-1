#ifndef core_h
#define core_h

#include <ap_int.h>
#include <ap_axi_sdata.h>
#include <hls_stream.h>

typedef ap_uint<16> word; 

#define NHEAD 6
#define NSAMPLES 64

typedef ap_axiu<16, 1, 0, 0> ap_axis_str;

void header_stripper(
	hls::stream<ap_axis_str> &input_stream,
	hls::stream<ap_axis_str> &header_stream,
	hls::stream<ap_axis_str> &data_stream);

#endif