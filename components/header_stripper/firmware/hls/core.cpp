#include "core.h"
#ifndef __SYNTHESIS__
#include <iostream>
#endif

void header_stripper(
	hls::stream<ap_axis_str> &input_stream,
	hls::stream<ap_axis_str> &header_stream,
	hls::stream<ap_axis_str> &data_stream){
	#pragma HLS INTERFACE axis port=input_stream
	#pragma HLS INTERFACE axis port=header_stream
	#pragma HLS INTERFACE axis port=data_stream
	#pragma HLS INTERFACE ap_ctrl_none port=return

	for (int i=0; i<NHEAD; i++){
		header_stream.write(input_stream.read());
	}
	for (int i=0; i<NSAMPLES; i++){
		data_stream.write(input_stream.read());
	}
}
