#include "core.h"
#include <iostream>

int main(){
	std::cout<<"\n===========================\n";
	hls::stream<ap_axis_str> input_stream;
	hls::stream<ap_axis_str> header_stream;
	hls::stream<ap_axis_str> data_stream;

	std::vector<word> data{43520,16384,21845,21845,21845,21845};
	std::vector<word> user{0,0,0,0,0,0};
	std::vector<word> last{0,0,0,0,0,1};

	for (int i=0; i<NSAMPLES; i++){
		data.push_back(i);
		user.push_back(0);
		last.push_back(0);
	}
	user[NHEAD+NSAMPLES-1] = 1;
	last[NHEAD+NSAMPLES-1] = 1;

	ap_axis_str write_value;
	for (int i=0; i<(NHEAD+NSAMPLES); i++){
		write_value.data = data[i];
		write_value.user = user[i];
		write_value.last = last[i];
		write_value.keep = 3;
		input_stream.write(write_value);
	}

	header_stripper(input_stream, header_stream, data_stream);

	while(!header_stream.empty()) {
		ap_axis_str strm_val_out;
		header_stream.read(strm_val_out);
		std::cout<<strm_val_out.data;
			if (strm_val_out.last) {
				std::cout<<"\n";
			}else if (strm_val_out.user){
				std::cout<<" || ";
			} else {
				std::cout<<", ";
			}
	}

	while(!data_stream.empty()) {
		ap_axis_str strm_val_out;
		data_stream.read(strm_val_out);
		std::cout<<strm_val_out.data;
			if (strm_val_out.last) {
				std::cout<<"\n";
			}else if (strm_val_out.user){
				std::cout<<" || ";
			} else {
				std::cout<<", ";
			}
	}
}
